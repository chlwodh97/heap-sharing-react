function Heap2(queue1, queue2) {
    let newArr = [...queue1, ...queue2];
    let count = 0;
    let redu = newArr.reduce((a,b) => a + b, 0)

    let target = [(redu) % 2 === 0]
    // 오름 차순
    newArr.sort((a, b) => a - b);

    // 배열 마지막 값
    let sum1 = newArr.length -1
    // 첫번째 값
    let sum2 = newArr[0]

    while (queue1.length > 0 && queue1.length > 0) {
        if (sum1+sum2 === target) {
            return count;
        } else if (sum1+sum2 <= target) {
            // 다음 배열 인덱스를 더해야함
            // 또 돌려 ?
            // 근데 누적도 해야함
            for (let i =0; newArr.length >= sum2[i]; i++) {
                sum1 += sum2[i];
            }
        } else if (sum1+sum2 >= target) {
            return "나나오면안댐"
        }
        count++
    }

    return sum1+sum2 === target ? count : -1;
}

console.log(Heap2([2,4],[3,5]));