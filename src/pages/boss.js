function Heap2(queue1, queue2) {
    // 배열 합치기
    let totalList = [...queue1, ...queue2];
    // totalSum은 합친 배열을 더한 값
    let totalSum = totalList.reduce((a,b) => a + b, 0)

    // tempSum 은 1배열을 합친 더 한 값
    let tempSum = queue1.reduce((a,b) => a + b,0)


    let leftPo = 0
    let rightPo = queue1.length -1
    let count = 0


    // L포인트가 <= R포인트 보다 크거나 같아 질때 까지 돌겠다는 뜻
    while (leftPo <= rightPo) {

        // 만약에 (배열1,2의 합 빼기- 1배열의 합이) == (그냥 1배열의 합) 이면 리턴횟수
        //   반반 나뉘면 리턴 카운터
        if (totalSum - tempSum === tempSum) return count;

        // 1배열의 합이 < 1,2배열 - 1 배열 보다 작으면
        // -> 1배열이 (총배열 - 1 배열) 보다 크다면
        if (tempSum < totalSum - tempSum) {
            // R포인트 오른쪽 이동
            rightPo ++
            // 1배열에 r포인트 값을 더해서 대입
            // -> 이말은 1배열의 값을 증가시킨다는 얘기
            tempSum += totalList[rightPo]
            // 카운트 샘
            count ++
        }
            // 1배열의 합이 > 1,2 배열 -1 배열보다 크면
            // -> 1배열이 (총배열 - 1배열) 보다 작다면
        else {
            // 1배열에 L포인트 값을 빼서 대입
            tempSum -= totalList[leftPo]
            // L포인트 오른쪽 이동
            leftPo++
            // 카운트 샘
            count++
        }
    }
    // 종료 조건이 지나도록 위에서 리턴을 하지 못하면
    // 그냥 조건에 맞지 않는 것이기 때문에 마이너스 1 리턴
    return -1;
}

console.log(Heap2([2,4],[3,5]));