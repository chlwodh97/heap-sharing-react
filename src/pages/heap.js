function Heap(queue1, queue2) {
    let sum1 = queue1.reduce((a,b) => a + b ,0);
    let sum2 = queue2.reduce((a,b) => a + b ,0);
    let count = 0;


    while (queue1.length > 0 && queue1.length > 0) {
        if (sum1 === sum2) {
            return count;
        } else if (sum1 > sum2) {
            let popVal = queue1.shift();
            queue2.push(popVal);
            sum1 -= popVal;
            sum2 += popVal;
        } else {
            let popVal = queue2.shift();
            queue1.push(popVal);
            sum1 += popVal;
            sum2 -= popVal;
        }
        count ++;
    }

    return sum1 === sum2 ? count : -1;
}